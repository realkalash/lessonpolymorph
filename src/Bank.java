public class Bank extends Market {
    private double limitUAH;

    public Bank(String name, double courseUSD, double limitUAH) {
        super(name, courseUSD);
        this.limitUAH = limitUAH;
    }

    public double getLimitUAH() {
        return limitUAH;
    }

    @Override
    public double convert(int inputUAH) {

        if (inputUAH < limitUAH) {
            return super.convert(inputUAH);
        }
        return 0;
    }
}
