public class Generator {


    protected static void GenerateExchangers() {
    }

    public Market[] GenerateMarkets() {

        Market[]markets= new Market[5];
        markets[0] = new Bank("Privat24", 26.5d, 5000d);
        markets[1] = new Exchanger("First Exchenger", 25d, 250d);
        markets[2] = new Exchanger("Second Exchenger", 25.6d, 300d);
        markets[3] = new Exchanger("Thirty Exchenger", 26.8d, 275d);
        markets[4] = new BlackMarket("BlackMarket", 28.5d);

        return markets;
    }
}
