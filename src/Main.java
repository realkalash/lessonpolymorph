import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // Extends
        // Agregation
        // Composition
        Generator generator = new Generator();
        Market[] markets = generator.GenerateMarkets();
        System.out.println("Выберите кол-во суммы в грн для конвертации");
        double max = 0;
        double min = 100000;

        Scanner scanner = new Scanner(System.in);
        int input = Integer.parseInt(scanner.nextLine());

        System.out.println("Выводим только Обменник:");
        for (Market item : markets) {
            if (item instanceof Exchanger) {

                System.out.println(item.convert(input));

            }
        }
        for (int i = 0; i < markets.length; i++) {

            if (markets[i].getCourseUSD() > max) {
                max = markets[i].getCourseUSD();
            }
        }

        for (int i = 0; i < markets.length; i++) {
            if (markets[i].getCourseUSD() < max && markets[i].getCourseUSD() < min) {
                min = markets[i].getCourseUSD();
            }
        }
        System.out.println("min: " + min);
    }
}