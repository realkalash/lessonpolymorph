public class Market {
    private String name;
    private double rate;

    public Market(String name, double courseUSD) {
        this.name = name;
        this.rate = courseUSD;
    }


    public double convert(int inputUAH) {
       return inputUAH / rate;
    }

    public String getName() {
        return name;
    }

    public double getCourseUSD() {
        return rate;
    }

}
